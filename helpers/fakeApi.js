
const cards = [
  {
    prices: [
      '1450000 k.k.',
    ],
    type: 'house',
    totalSquare: 900,
    square: 210,
    rooms: 3,
    bed: 2,
    street: 'Antonia Korvezeetuin 21',
    region: 'Amsterdam, Nederland',
  },
  {
    prices: [
      '1450000 k.k.',
    ],
    type: 'flat',
    totalSquare: 900,
    square: 210,
    rooms: 3,
    bed: 2,
    street: 'Antonia Korvezeetuin 21',
    region: 'Amsterdam, Nederland',
  },
  {
    prices: [
      '1450000 k.k.',
    ],
    type: 'garage',
    square: 210,
    street: 'Antonia Korvezeetuin 21',
    region: 'Amsterdam, Nederland',
  },
  {
    prices: [
      '1450000 k.k.',
    ],
    type: 'area',
    totalSquare: 900,
    street: 'Antonia Korvezeetuin 21',
    region: 'Amsterdam, Nederland',
  },
];

const about = {
  about: {
    heading: 'About Bloemendaal makelaars &amp; taxateurs Enkhuizen',
    text: 'Bloemendaal Makelaars &amp; Taxateurs is onderdeel van de Bloemendaal Adviesgroep. Bloemendaal is ruim 25 jaar een begrip in West-Friesland en Lelystad op het gebied van makelaardij, hypotheken en verzekeringen. Met vestigingen in Hoorn, Medemblik, Stede Broec, Enkhuizen en Lelystad is er altijd een collega van Bloemendaal dichtbij voor u. Door onze grote regionale kennis, persoonlijke aanpak en onze succesvolle verkoopmethode, slaagt u altijd om het beste resultaat te behalen, voor ú.<br><br>Onze makelaars staan u persoonlijk bij in het hele koop- of verkooptraject. Met makelaarskantoren in Hoorn, Enkhuizen en Lelystad heeft u een uitgebreide keuze en een groot verkoopkanaal.',
  },
  team: [
    {
      name: 'Jeroen Westgeest',
      link: '#',
    },
    {
      name: 'Kristel Roozendaal',
      link: '#',
    },
    {
      name: 'Henk Westgeest',
      link: '#',
    },
    {
      name: 'Henk Westgeest',
      link: '#',
    },
  ],
};

const apiCall = function apiCall() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        cards,
        about,
      });
    }, 1000);
  });
};

export default apiCall;
