/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors
const state = () => ({
  companies: {
    settings: false,
  },
  loading: false,
});

const mutations = {
  toggleSettingsPopup(state) {
    state.companies.settings = !state.companies.settings;
  },
  setSettingsPopup(state, value) {
    state.companies.settings = value;
  },
  closeAll(state, type) {
    Object.keys(state[type]).forEach((key) => {
      state[type][key] = false;
    });
  },
  showLoading(state) {
    state.loading = true;
  },
  hideLoading(state) {
    state.loading = false;
  }
};

const getters = {
  
};

const actions = {
  formatPlace({}, data) {
    return new Promise((resolve, reject) => {
    const newAddress = `adress`;
      resolve(newAddress);
    })
  },
  formatUrl({}, url) {
    return new Promise((resolve, reject) => {
      const arr = url.split('');
      const newArr = arr.map((a) => {
        if (a === ' ' || a === '.' || a === ',') {
          return '-';
        }
        return a;
      })
      const newUrl = newArr.join('').toLowerCase();
      resolve(newUrl);
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
