/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors
import http from 'axios';
import { api } from '@/api/admin';

const state = () => ({
  personal: {
    name: '',
    surname: '',
    current_lang: '',
    email: '',
    phone: '',
    password: '',
    avatar: '',
    id: '',
  },
});

const mutations = {
  setValue(state, [type, value]) {
    state.personal[type] = value;
  },
  deleteValue(state, type) {
    state.personal[type] = '';
  },
  updateData(state, data) {
    state.personal = Object.assign({}, state.personal, data);
  },
};

const getters = {
  
};

const actions = {
  getData({ commit }) {
    http.post(api.me).then((response) => {
      if (response.status === 200) {
        commit('updateData', response.data.user);
      }
    })
  },
  updateProfile({ state }, data) {
    return new Promise((resolve, reject) => {
      http.put(api.profile(state.personal.id), data)
        .then((response) => {
          if (response.status === 200) {
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        })
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
