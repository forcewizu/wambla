/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors

const state = () => ({
  crumbs: [
    {
      name: 'Real estate agencies',
      route: '/'
    },
    {
      name: 'Noord-Holland',
      route: '/'
    },
    {
      name: 'Enkhuizen',
      route: '/'
    },
    {
      name: 'Bloemendaal makelaars & taxateurs Enkhuizen',
      route: '/'
    },
  ],
});

const mutations = {
  pushCrumbs(state, crumb) {
    state.crumbs.push(crumb);
  },
  updateCumbs(state, index) {
    state.crumbs.splice(index);
  },
};

const getters = {
  
};

const actions = {
  addCrumb({ commit, state }, crumb) {
    const index = state.crumbs.findIndex((item) => {
      return item[route] === crumb[route];
    });
    if (index >= 0) {
      commit('updateCumbs', index);
    } else {
      commit('pushCrumbs', crumb);
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
