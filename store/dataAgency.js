/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors
// import apiCall from '@/helpers/fakeApi';
import http from 'axios';
import { api } from '@/api/public';
import { objectDetails } from './utils/full';

const schedules = [
  {name: 'monday', closed: false, start_time: {HH: '08', mm: '00'}, end_time: {HH: '18', mm: '00'}},
  {name: 'tuesday', closed: false, start_time: {HH: '08', mm: '00'}, end_time: {HH: '18', mm: '00'}},
  {name: 'wednesday', closed: false, start_time: {HH: '08', mm: '00'}, end_time: {HH: '18', mm: '00'}},
  {name: 'thursday', closed: false, start_time: {HH: '08', mm: '00'}, end_time: {HH: '18', mm: '00'}},
  {name: 'friday', closed: false, start_time: {HH: '08', mm: '00'}, end_time: {HH: '18', mm: '00'}},
  {name: 'saturday', closed: true, start_time: {HH: '', mm: ''}, end_time: {HH: '', mm: ''}},
  {name: 'sunday', closed: true, start_time: {HH: '', mm: ''}, end_time: {HH: '', mm: ''}},
];

const state = () => ({
  objectDetails: {...objectDetails},
  about: {
    title: '',
    text: '',
  },
  shareLinks: [],
  team: [],
  cards: {
    forSale: [],
    sold: [],
    forRent: [],
    rented: [],
  },
  objects: [],
  user: {
    name: '',
    surname: '',
    avatar: '',
    role: '',
    email: '',
    phone: '',
    created_at: '',
    updated_at: '',
    id: '',
  },
  company: {
    id: '',
    url: '',
    logo: '',
    status: '',
    c_name: '',
    official_c_name: '',
    сhamber_of_commerce: '',
    VAT: '',
    extra: '',
    extra_nl: '',
    extra_en: '',
    phones: [],
    emails: [],
    languages: [],
    socials: [],
    files: [],
    user: {
      name: '',
      surname: '',
      avatar: '',
      role: '',
      email: '',
      phone: '',
      created_at: '',
      updated_at: '',
      id: '',
    },
    website: '',
    address: '',
    location: '',
    headerPhoto: '',
    latitude: '',
    longitude: '',
    created_at: '',
    updated_at: '',
    description: '',
    description_big: '',
    description_nl: '',
    description_big_nl: '',
    description_en: '',
    description_big_en: '',
    teams: [],
    schedules: schedules,
  },
});

const mutations = {
  updateData(state, data) {
    Object.assign(state, data);
  },
  updateCards(state, data) {
    Object.keys(state.cards).forEach((key) => {
      state.cards[key] = [...data];
    })
  },
  updateObjects(state, data) {
    state.objects = [...data];
  },
  updateUser(state, data) {
    state.user = Object.assign({}, state.user, data);
  },
  updateCompany(state, data) {
    state.company = Object.assign({}, state.company, data);
    if (!state.company.schedules.length) {
      state.company.schedules = [...schedules];
    }
  },
  updateAbout(state, data) {
    state.about = Object.assign({}, state.about, data.about);
    state.team = [...data.team];
  },
  setCompanyValue(state, [type, data]) {
    state.company[type] = data;
  },
  addMember(state, data) {
    state.company.teams.push(data);
  },
  updateMember(state, data) {
    const index = state.company.teams.findIndex(e => e.id === data.id);
    if (data.new) {
      state.company.teams.splice(index, 1);
    } else {
      const newData = Object.assign({}, data.backup, {backup: data.backup});
      state.company.teams.splice(index, 1, newData);
    }
  },
  setMemberValue(state, [id, type, value]) {
    const index = state.company.teams.findIndex(e => e.id === id);
    const data = Object.assign({}, state.company.teams[index], {[type]: value});
    state.company.teams.splice(index, 1, data);
  },
  backupTeam(state, data) {
    state.company.teams = [...data];
  },
  backupMember(state, data) {
    const index = state.company.teams.findIndex(e => e.id === data.id);
    const backup = Object.assign({}, data);
    delete backup.backup;
    state.company.teams[index].new = false;
    state.company.teams[index].backup = Object.assign({}, backup);
  },
  checkSchedule(state) {
    if (!state.company.schedules.length) {
      state.company.schedules = [...schedules];
    }
  },
  deleteMemberLocal(state, index) {
    state.company.teams.splice(index, 1);
  },
  updateObjectDetails(state, data) {
    const{ all_body_object = {}} = data;
    const full = JSON.parse(all_body_object);
    data.all_body_object = null;
    data.full = Object.assign({}, objectDetails.full, full);
    state.objectDetails = Object.assign({}, state.objectDetails, data);
  },
  editSchedulesValue(state, { key, value, idx }) {
    const day = state.company.schedules[idx];
    state.company.schedules.splice(idx, 1, {...day, [key]: value});
  }
};

const getters = {
  getCountObjects: (state) => {
    return state.objects.length;
  },
  getFavoritePhone: (state) => {
    const phone = state.company.phones.find(item => item.favorite);
    if (phone) {
      return phone.phone;
    } else if (state.company.phones[0]) {
      return state.company.phones[0].phone;
    } else if (state.user.phone) {
      return state.user.phone;
    }
    return '';
  },
  getFavoriteEmail: (state) => {
    const email = state.company.emails.find(item => item.favorite);
    if (email) {
      return email.email;
    } else if (state.company.emails[0]) {
      return state.company.emails[0].email;
    }
    return state.user.email;
  },
  team: (state) => {
    let data = [...state.company.teams];
    return data.sort((a, b) => a.order_id - b.order_id)
  },
  getForSale: (state) => {
    return state.objects.filter(({ sale }) => sale) || [];
  },
  getSold: (state) => {
    return state.objects.filter(({ sold = false }) => sold) || [];
  },
  getForRent: (state) => {
    return state.objects.filter(({ rent }) => rent) || [];
  },
  getRented: (state) => {
    return state.objects.filter(({ rented = false }) => rented) || [];
  },
  checkObject: (state) => (byId) => {
    return state.objects.some(({id}) => Number(id) === Number(byId));
  },
  getObject: (state) => (byId) => {
    return state.objects.find(({id}) => Number(id )=== Number(byId));
  },
  getCompanyName: (state) => state.company.c_name,
  getCompanyRoute: (state) => `/${state.company.id}`,
  getObjectName: (state) => {
    return state.objectDetails.address;
  },
  getObjectRoute: (state) => {
    return `/${state.company.id}/${state.objectDetails.id}`;
  },
};

const actions = {
  getObjectDetails({commit, getters, dispatch}, {id, url}) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      dispatch('getCompanyObjectsAsync', url)
        .then((company_id) => {
          const formData = Object.assign({}, {company_id, object_id: Number(id)});
          dispatch('getFullObject', formData)
            .then(() => {
              commit('gears/hideLoading', null, {root: true});
              resolve()
            })
            .catch(() => {
              commit('gears/hideLoading', null, {root: true});
              reject();
            })
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          reject();
        })
    })
  },
  getObject({ commit }, id) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.get(api.getObject(id))
        .then(({ data }) => {
          commit('gears/hideLoading', null, {root: true});
          commit('updateObjectDetails', data.data);
          resolve(data.data.company_id);
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          resolve(null);
        })
    })
  },
  getFullObject({ commit }, {company_id, object_id}) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.get(api.getApiFullObject(company_id, object_id))
        .then(({data}) => {
          const { 0: objA } = data.obj;
          const result = objA ? objA : data.data[0];
          commit('gears/hideLoading', null, {root: true});
          commit('updateObjectDetails', result);
          resolve();
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          reject(error);
        })
    })
  },
  async getCompanyObjectsAsync({ dispatch, commit }, url) {
    commit('gears/showLoading', null, {root: true});
    const id = await dispatch('getCompanyByUrl', url);
    await dispatch('getObjectsAsync', id);
    commit('gears/hideLoading', null, {root: true});
    return id;
  },
  backupTeam({ state, commit }) {
    const data = state.company.teams.map((e) => {
      e.backup = Object.assign({}, e);
      return e;
    })
    commit('backupTeam', data);
  },
  getCompanyByUrl({ commit, dispatch }, url) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.post(api.companyByUrl, {url})
        .then(({data}) => {
          commit('gears/hideLoading', null, {root: true});
          commit('updateCompany', data.data);
          commit('updateUser', data.data.user);
          dispatch('getObjects', data.data.id);
          resolve(data.data.id);
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          reject(error);
        })
    });
  },
  getCompanyById({ commit, dispatch }, id) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.get(api.companyById(id))
        .then(({data}) => {
          commit('gears/hideLoading', null, {root: true});
          commit('updateCompany', data.data);
          commit('updateUser', data.data.user);
          dispatch('getObjects', data.data.id);
          resolve(data.data.url);
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          resolve(null);
        })
    });
  },
  getObjectsAsync({ commit }, id) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      const data = {
        params: {
          company_id: id,
        },
      };
      http.get(api.objects, data)
        .then(({data}) => {
          commit('gears/hideLoading', null, {root: true});
          commit('updateObjects', data.data);
          resolve();
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          reject(error)
        })
    })

  },
  getObjects({ commit }, id) {
    commit('gears/showLoading', null, {root: true});
    const data = {
      params: {
        company_id: id,
      },
    };
    http.get(api.objects, data)
      .then(({data}) => {
        commit('updateObjects', data.data);
        commit('gears/hideLoading', null, {root: true});
      })
      .catch((error) => {
        commit('gears/hideLoading', null, {root: true});
      })
  },
  addMember({ state, commit }) {
    let order = 1;
    if (state.company.teams.length) {
      const arr = state.company.teams.map(e => e.order_id);
      order = Math.max( ...arr ) + 1;
    }
    const id = Math.random() * (99999999999999 - 0.00000001) + 0.00000001;
    const data = {
      company_id: state.company.id,
      order_id: order,
      name: '',
      id: id,
      job: '',
      avatar_team: '',
      new: true,
      backup: {},
    };
    commit('addMember', data);
  },
  changeOrder({ state, commit }, [id, type, value]) {
    const order = state.company.teams.some(e => e.order_id === value);
    if (order) {
      const newValue = state.company.teams.find(e => e.id === id);
      commit('setMemberValue', [id, type, newValue.order_id]);
    } else {
      commit('setMemberValue', [id, type, value]);
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
