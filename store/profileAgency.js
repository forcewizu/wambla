/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors
import http from 'axios';
import { api } from '@/api/agency';
import apiCall from '@/helpers/fakeApi';

const state = () => ({
  personal: {
    name: '',
    surname: '',
    email: '',
    phone: '',
    password: '',
    avatar: '',
    secret: '',
    id: '',
  },
  company: {
    pageUrl: '',
    id: '',
    url: '',
    logo: '',
    status: '',
    c_name: '',
    official_c_name: '',
    сhamber_of_commerce: '',
    VAT: '',
    extra: '',
    extra_nl: '',
    extra_en: '',
    phones: [],
    files: [],
    emails: [],
    languages: [],
    socials: [],
    website: '',
    address: '',
    location: '',
    headerPhoto: '',
    latitude: '',
    longitude: '',
    description: '',
    description_big: '',
    description_nl: '',
    description_big_nl: '',
    description_en: '',
    description_big_en: '',
    teams: [],
  },
  landing: {
    logo: [],
    team: [],
    about: [],
    schedule: [],
  },
});

const mutations = {
  updateLanding(state, data) {
    state.landing = Object.assign({}, state.landing, data);
  },
  setCompanyValue(state, [type, value]) {
    state.company[type] = value;
  },
  setPersonalValue(state, [type, value]) {
    state.personal[type] = value;
  },
  pushValue(state, [type, value]) {
    state.company[type].push(value);
  },
  clearArray(state, type) {
    state.company[type] = [];
  },
  deleteValue(state, [type, index]) {
    state.company[type].splice(index, 1);
  },
  removeEmail(state, index) {
    state.company.emails.splice(index, 1);
  },
  removePhone(state, index) {
    state.company.phones.splice(index, 1);
  },
  removeSocial(state, index) {
    state.company.socials.splice(index, 1);
  },
  removeLogo(state) {
    state.company.logo = '';
  },
  removeLang(state, code) {
    const idx = state.company.languages.findIndex(e => e.code === code);
    state.company.languages.splice(idx, 1);
  },
  addEmail(state) {
    const data = {
      id: Math.floor(Math.random() * 1000000) + 1,
      email: '',
      is_check: 0,
      favorite: !state.company.emails.length,
    };
    state.company.emails.push(data);
  },
  addLanguage(state, lang) {
    const data = Object.assign({status: true}, lang);
    state.company.languages.push(data);
  },
  addPhone(state) {
    const data = {
      id: Math.floor(Math.random() * 1000000) + 1,
      phone: '',
      country: 'nl',
      is_check: false,
      favorite: !state.company.phones.length,
      drop: false,
    };
    state.company.phones.push(data);
  },
  updateDocs(state, files) {
    state.company.files = [...files];
  },
  addSocial(state) {
    const data = {
      id: Math.floor(Math.random() * 1000000) + 1,
      type: 'facebook',
      username: '',
      drop: false,
    };
    state.company.socials.push(data);
  },
  toggleFavoritePhone(state, id) {
    state.company.phones.forEach((el) => {
      if (el.id === id) {
        el.favorite = !el.favorite; 
      } else {
        el.favorite = false; 
      }
    });
  },
  toggleFavoriteEmail(state, id) {
    state.company.emails.forEach((el) => {
      if (el.id === id) {
        el.favorite = !el.favorite; 
      } else {
        el.favorite = false; 
      }
    });
  },
  updatePersonalData(state, data) {
    state.personal = Object.assign({}, state.personal, data);
  },
  updateData(state, data) {
    Object.assign(state, data);
  },
  updateCompany(state, data) {
    if (data.phones.length) {
      data.phones.forEach((item) => {
        item.drop = false;
        item.favorite = Number(item.favorite);
        item.is_check = Number(item.is_check);
      });
    };
    if (data.socials.length) {
      data.socials.forEach((item) => {
        item.drop = false;
        item.favorite = Number(item.favorite);
        item.is_check = Number(item.is_check);
      });
    };
    if (data.languages.length) {
      data.languages.forEach((item) => {
        item.status = Number(item.status);
      });
    };
    state.company = Object.assign({}, state.company, data);
  },
  addMember(state, data) {
    state.company.teams.push(data);
  },
  setPhoneValue(state, [val, idx]) {
    state.company.phones[idx].phone = val;
  },
  setEmailValue(state, [val, idx]) {
    state.company.emails[idx].email = val;
  },
  closePhoneDrop(state, payload) {
    const idx = state.company.phones.findIndex(({ id }) => payload === id);
    state.company.phones[idx].drop = false;
  },
};

const getters = {
  agencyId: state => state.company.id?state.company.id:null,
  getFavoriteEmail: (state) => {
    const email = state.company.emails.find(item => item.favorite);
    if (email) {
      return email.email;
    } else if (state.company.emails[0]) {
      return state.company.emails[0].email;
    }
    return state.personal.email;
  },
  getLanguages: (state) => {
    if (state.company.languages.length) {
      return state.company.languages.filter(e => e.status);
    }
    return [];
  },
};

const actions = {
  getLanding({ commit }) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.get(api.landing)
        .then((resp) => {
          commit('gears/hideLoading', null, {root: true});
          commit('updateLanding', resp.data);
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          reject(error);
        })
    });
  },
  pushValue({ state, commit }, [type, value]) {
    const isValue = state.company[type].some((item) => {
      return item === value;
    });
    if (!isValue) {
      commit('pushValue', [type, value]);
    }
  },
  updatePhones({ state }) {
    const phones = [...state.company.phones];
    const formData = Object.assign({phones: JSON.stringify(phones)}, {company_id: state.company.id});
    return new Promise((resolve, reject) => {
      http.post(api.updatePhones, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  deleteValue({ state, commit }, value) {
    const index = state.company[type].findIndex((item) => {
      return item === value;
    });
    commit('deleteValue', [type, index]);
  },
  updateArrays({ dispatch }) {
    return new Promise((resolve, reject) => {
      dispatch('updatePhones')
      .then(() => {
        dispatch('updateSocials')
          .then(() => {
            dispatch('updateLanguages')
              .then(() => {
                dispatch('updateEmails')
                  .then(() => resolve())
                  .catch(() => reject())
              })
              .catch(() => reject())
          })
          .catch(() => reject())
      })
      .catch(() => reject())
    }); 
  },
  getData({ commit }) {
    commit('gears/showLoading', null, {root: true});
    http.post(api.me).then((response) => {
      if (response.status === 200) {
        commit('gears/hideLoading', null, {root: true});
        commit('updatePersonalData', response.data.user);
        commit('updateCompany', response.data.company);
      }
    }).catch((error) => {
      commit('gears/hideLoading', null, {root: true});
      console.error(`${api.me} => ${error}`);
    })
  },
  getDataPromise({ commit }) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.post(api.me).then((response) => {
        if (response.status === 200) {
          commit('gears/hideLoading', null, {root: true});
          commit('updatePersonalData', response.data.user);
          commit('updateCompany', response.data.company);
          resolve();
        } else {
          resolve(false);
        }
      })
      .catch((error) => {
        commit('gears/hideLoading', null, {root: true});
        console.error(`${api.me} => ${error}`);
        resolve(false);
      })
    });
  },
  getCompanyById({ commit }) {
    commit('gears/showLoading', null, {root: true});
    http.get(api.updateData(state.company.id))
      .then((response) => {
        commit('gears/hideLoading', null, {root: true});
        if (response.status === 200) {
          commit('updateCompany', response.data.data);
        }
      }).catch((error) => {
        commit('gears/hideLoading', null, {root: true});
      })
  },
  getCompanyByIdPromise({ state, commit }) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.get(api.updateData(state.company.id))
      .then((response) => {
        commit('gears/hideLoading', null, {root: true});
        if (response.status === 200) {
          commit('updateCompany', response.data.data);
          resolve();
        } else {
          resolve();
        }
      }).catch((error) => {
        commit('gears/hideLoading', null, {root: true});
        console.error(`${api.updateData(state.company.id)} => ${error}`);
        resolve()
      });
    });
  },
  async updateData({ state, dispatch }) {
    const url = await dispatch('gears/formatUrl', state.company.url, { root: true });
    return new Promise((resolve, reject) => {
      
      const data = {
          c_name: state.company.c_name,
          official_c_name: state.company.official_c_name,
          VAT: state.company.VAT,
          address: state.company.address,
          website: state.company.website,
          extra: state.company.extra,
          сhamber_of_commerce: state.company.сhamber_of_commerce,
          description_en: state.company.description_en,
          description_big_en: state.company.description_big_en,
          description_nl: state.company.description_nl,
          description_big_nl: state.company.description_big_nl,
          latitude: state.company.latitude,
          longitude: state.company.longitude,
      };
      http.put(api.updateData(state.company.id), data)
        .then((resp) => {
          if (resp.status === 200) {
            dispatch('updateArrays')
              .then(() => resolve(resp))
              .catch(() => reject(resp));
          } else {
            reject(resp);
          }
        })
        .catch((error) => reject(error));
    })
  },
  putData({ state }, data) {
    return new Promise((resolve, reject) => {
      http.put(api.updateData(state.company.id), data)
        .then((resp) => {
          if (resp.status === 200) {
            resolve(resp);
          } else {
            reject(resp);
          }
        })
        .catch((error) => reject(error));
    })
  },
  updateProfile({ state }, data) {
    return new Promise((resolve, reject) => {
      http.put(api.profile(state.personal.id), data)
        .then((response) => {
          if (response.status === 200) {
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        })
    });
  },
  updateSocials({ state }) {
    const socials = [...state.company.socials]
    const formData = Object.assign({
      socials: JSON.stringify(socials),
      company_id: state.company.id,
    });
    return new Promise((resolve, reject) => {
      http.post(api.updateSocials, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  updateEmails({ state }) {
    const emails = [...state.company.emails];
    const formData = Object.assign({}, {
      emails: JSON.stringify(emails),
      company_id: state.company.id,
    });
    return new Promise((resolve, reject) => {
      http.post(api.updateEmails, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  updateLanguages({ state }) {
    const languages = [...state.company.languages];
    const formData = Object.assign({}, {
      languages: JSON.stringify(languages),
      company_id: state.company.id,
    });
    return new Promise((resolve, reject) => {
      http.post(api.updateLanguages, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  getDocsAsync({ commit }) {
    return new Promise((resolve, reject) => {
      http.post(api.me).then((response) => {
        if (response.status === 200) {
          const { files } = response.data.company;
          commit('updateDocs', files);
          resolve();
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      })
    });
  },
  getDataPure() {
    return new Promise((resolve, reject) => {
      http.post(api.me).then((response) => {
        if (response.status === 200) {
          resolve(response.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error);
      })
    });
  },
  addMember({ state, commit }) {
    const order = 1;
    if (state.company.teams.length) {
      const arr = state.company.teams.map(e => e.order_id);
      order = Math.max( ...arr ) + 1;
    }
    const data = {
      company_id: state.company.id,
      order_id: order,
      name: '',
      job: '',
      avatar: '',
    };
    commit('addMember', data);
  },
  pushMember({}, data) {
    return new Promise((resolve, reject) => {
      http.post(api.team, data).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    });
  },
  updateSchedule({ state }, data) {
    const formData = Object.assign({}, {company_id: state.company.id}, {schedule: data});
    return new Promise((resolve, reject) => {
      http.post(api.schedule, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    });
  },
  deleteMember({}, data) {
    return new Promise((resolve, reject) => {
      http.post(api.memberDelete, data).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
