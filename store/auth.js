/* eslint-disable */

import http from 'axios';
import { api } from '@/api/agency';

const state = () => ({
  // token: process.client ? localStorage.getItem('wambla-user-token') || '' : '',
  // tokenAdmin: process.client ? localStorage.getItem('wambla-admin-token') || '' : '',
  token: '',
  tokenAdmin: '',
  authStatus: '',
  userStatus: '',
  role: '',
  user: {},
  agencyId: '',
});

const mutations = {
  AUTH_REQUEST: state => {
    state.authStatus = 'loading';
  },
  AUTH_SUCCESS: (state, token) => {
    state.token = token;
    state.authStatus = 'success';
  },
  AUTH_SUCCESS_ADMIN: (state, token) => {
    state.tokenAdmin = token;
    state.authStatus = 'success';
  },
  AUTH_ERROR: state => {
    state.token = '';
    state.tokenAdmin = '';
    state.authStatus = 'error';
  },
  USER_REQUEST: state => {
    state.userStatus = 'loading';
  },
  USER_SUCCESS: (state, data) => {
    state.user = data;
    state.userStatus = 'success';
  },
  USER_ERROR: state => {
    state.userStatus = 'error';
  },
  AUTH_LOGOUT: state => {
    state.token = '';
    state.tokenAdmin = '';
  },
  toggleKeepingUser( state ) {
    state.keepUser = !state.keepUser;
  },
  setTokens(state, { token = '', tokenAdmin = '' }) {
    state.token = token;
    state.tokenAdmin = tokenAdmin;
  },
  SET_ROLE(state, payload) {
    state.role = payload;
  }
};

const getters = {
  isAuthenticated: state => !!state.token,
  isAdminAuthenticated: state => !!state.tokenAdmin,
  isAuth: state => !!state.token || !!state.tokenAdmin,
  authStatus: state => state.authStatus,
};

const actions = {
  SIGNUP: ({ commit }, { formData, vm }) => {
    return new Promise((resolve, reject) => {
      http.post(api.signup, formData).then(({data}) => {
        const { access_token: token } = data;
        http.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        vm.$cookies.set('wambla-user-token', token);
        commit('AUTH_SUCCESS', token);
        commit('USER_SUCCESS', data);
        resolve();
      }).catch((error) => {
        reject(error);
      })
    })
    
  },
  AUTH_REQUEST: ({ commit, dispatch }, { user, vm }) => {
    return new Promise((resolve, reject) => {
      commit('AUTH_REQUEST');
      commit('USER_REQUEST');
      http.post(api.login, user)
      .then((response) => {
        const { access_token: token } = response.data;
        const { user: data } = response.data;
        const { role } = response.data.user;
        commit('SET_ROLE', role);
        http.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        
        if (response.status === 200) {
          if (role === 'admin') {
            vm.$cookies.set('wambla-admin-token', token);
            commit('AUTH_SUCCESS_ADMIN', token);
            commit('USER_SUCCESS', data);
            resolve('admin');
          } else {
            vm.$cookies.set('wambla-user-token', token);
            commit('AUTH_SUCCESS', token);
            commit('USER_SUCCESS', data);
            resolve('user');
          } 
        } else {
          vm.$cookies.remove('wambla-user-token');
          vm.$cookies.remove('wambla-admin-token');
          reject(response);
        }
      })
      .catch((error) => {
        vm.$cookies.remove('wambla-user-token');
        vm.$cookies.remove('wambla-admin-token');
        commit('AUTH_ERROR');
        reject(error.response);
      })
    });
  },
  AUTH_LOGOUT: ({ commit }, { vm }) => {
    return new Promise(resolve => {
      commit('AUTH_LOGOUT');
      vm.$cookies.remove('wambla-user-token');
      vm.$cookies.remove('wambla-admin-token');
      resolve();
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
