export const state = () => ({});

export const mutations = {};

export const getters = {}

export const actions = {
  async nuxtServerInit ({ commit, dispatch }, { app }) {
    const token = app.$cookies.get('wambla-user-token') || '';
    const tokenAdmin = app.$cookies.get('wambla-admin-token') || '';
    const isAuth = token || tokenAdmin;
    const lang = app.$cookies.get('wambla-lang') || 'en';
    commit('auth/setTokens', { token, tokenAdmin }, { root: true });
    commit('language/setDefaultLang', lang, { root: true });
    if (isAuth) {
      try {
        dispatch('language/getLang', { vm: app }, { root: true });
        await dispatch('profileAgency/getDataPromise', null, { root: true });
        await dispatch('profileAgency/getCompanyByIdPromise', null, { root: true });
        return;
      } catch (err) {
        return;
      }
    }
  },
}
