/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors
const state = () => ({
  personal: {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    password: '',
  },
  company: {
    pageUrl: '',
    logo: '',
    companyName: '',
    companyOffName: '',
    chamber: '',
    vat: '',
    documents: [],
    phones: [],
    emails: [],
    languages: [],
    socials: [],
    site: '',
    address: '',
    location: '',
    headerPhoto: '',
  },
});

const mutations = {
  setCompanyValue(state, [type, value]) {
    state.company[type] = value;
  },
  setPersonalValue(state, [type, value]) {
    state.personal[type] = value;
  },
  pushValue(state, [type, value]) {
    state.company[type].push(value);
  },
  clearArray(state, type) {
    state.company[type] = [];
  },
  deleteValue(state, [type, index]) {
    state.company[type].splice(index, 1);
  },
  removeEmail(state, index) {
    state.company.emails.splice(index, 1);
  },
  removePhone(state, index) {
    state.company.phones.splice(index, 1);
  },
  removeSocial(state, index) {
    state.company.socials.splice(index, 1);
  },
  removeLogo(state) {
    state.company.logo = '';
  },
  removeDoc(state, index) {
    state.company.documents.splice(index, 1);
  },
  addEmail(state) {
    const data = {
      email: '',
      check: false,
      favorite: false,
    };
    state.company.emails.push(data);
  },
  addLanguage(state, lang) {
    const data = Object.assign({status: true}, lang);
    state.company.languages.push(data);
  },
  addPhone(state) {
    const data = {
      phone: '',
      country: 'nl',
      check: false,
      favorite: false,
      drop: false,
    };
    state.company.phones.push(data);
  },
  addSocial(state) {
    const data = {
      type: 'facebook',
      username: '',
      drop: false,
    };
    state.company.socials.push(data);
  },
  updateData(state, data) {
    Object.assign(state, data);
  },
};

const getters = {
  
};

const actions = {
  pushValue({ state, commit }, [type, value]) {
    const isValue = state.company[type].some((item) => {
      return item === value;
    });
    if (!isValue) {
      commit('pushValue', [type, value]);
    }
  },
  deleteValue({ state, commit }, value) {
    const index = state.company[type].findIndex((item) => {
      return item === value;
    });
    commit('deleteValue', [type, index]);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
