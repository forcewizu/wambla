/* eslint-disable */
// Because 'no-shadow' and 'no-param-reassing' errors of state aren't errors
import http from 'axios';
import { api } from '@/api/admin';
import moment from 'moment-timezone';

const state = () => ({
  companies: [],
  showId: '',
  company: {
    pageUrl: '',
    url: '',
    logo: '',
    status: '',
    c_name: '',
    official_c_name: '',
    сhamber_of_commerce: '',
    VAT: '',
    phones: [],
    emails: [],
    files: [],
    settings: [],
    languages: [],
    socials: [],
    website: '',
    address: '',
    location: '',
    headerPhoto: '',
    latitude: '',
    longitude: '',
    description: '',
    description_big: '',
    user: {
      name: '',
      surname: '',
      email: '',
      phone: '',
      avatar: '',
      id: '',
      role: '',
    }
  },
  personal: {
    name: '',
    surname: '',
    email: '',
    phone: '',
    password: '',
    avatar: '',
    id: '',
    updated_at: '',
    role: '',
  },
  search: '',
});

const mutations = {
  setSearch(state, value) {
    state.search = value;
  },
  setCompanyValue(state, [type, value]) {
    state.company[type] = value;
  },
  setPersonalValue(state, [type, value]) {
    state.personal[type] = value;
  },
  clearArray(state, type) {
    state.company[type] = [];
  },
  pushValue(state, [type, value]) {
    state.company[type].push(value);
  },
  addCompany(state, data) {
    state.companies.push(data);
  },
  updateCompanies(state, data) {
    state.companies = [...data];
  },
  updateCompany(state, data) {
    if (data.phones.length) {
      data.phones.forEach((item) => {
        item.drop = false;
        item.favorite = Number(item.favorite);
        item.is_check = Number(item.is_check);
      });
    };
    if (data.socials.length) {
      data.socials.forEach((item) => {
        item.drop = false;
        item.favorite = Number(item.favorite);
        item.is_check = Number(item.is_check);
      });
    };
    if (data.languages.length) {
      data.languages.forEach((item) => {
        item.status = Number(item.status);
      });
    };
    state.company = Object.assign({}, state.company, data);
  },
  setId(state, id) {
    state.showId = id;
  },
  addEmail(state) {
    const data = {
      id: Math.floor(Math.random() * 1000000) + 1,
      email: '',
      is_check: false,
      favorite: !state.company.emails.length,
    };
    state.company.emails.push(data);
  },
  addLanguage(state, lang) {
    const data = Object.assign({status: true}, lang);
    state.company.languages.push(data);
  },
  addPhone(state) {
    const data = {
      id: Math.floor(Math.random() * 1000000) + 1,
      phone: '',
      country: 'nl',
      is_check: false,
      favorite: !state.company.phones.length,
      drop: false,
    };
    state.company.phones.push(data);
  },
  addSocial(state) {
    const data = {
      id: Math.floor(Math.random() * 1000000) + 1,
      type: 'facebook',
      username: '',
      drop: false,
    };
    state.company.socials.push(data);
  },
  removeLang(state, code) {
    const idx = state.company.languages.findIndex(e => e.code === code);
    state.company.languages.splice(idx, 1);
  },
  removeEmail(state, index) {
    state.company.emails.splice(index, 1);
  },
  removePhone(state, index) {
    state.company.phones.splice(index, 1);
  },
  removeLogo(state) {
    state.company.logo = '';
  },
  removeSocial(state, index) {
    state.company.socials.splice(index, 1);
  },
  updatePersonalData(state, data) {
    state.personal = Object.assign({}, state.personal, data);
  },
  toggleFavoritePhone(state, id) {
    state.company.phones.forEach((el) => {
      if (el.id === id) {
        el.favorite = !el.favorite;
      } else {
        el.favorite = false;
      }
    });
  },
  updateDocs(state, files) {
    state.company.files = [...files];
  },
  toggleFavoriteEmail(state, id) {
    state.company.emails.forEach((el) => {
      if (el.id === id) {
        el.favorite = !el.favorite;
      } else {
        el.favorite = false;
      }
    });
  },
  toggleCheckedCompanies(state, value) {
    state.companies = state.companies.map((item) => {
      item.checked = value;
      return item;
    })
  },
  sortName(state) {
    const max = 'ÿ'.charCodeAt();
    if (state.companies.length > 1) {
      state.companies.sort((a, b) => {
        const x = String(a.c_name)?String(a.c_name).toLowerCase().charCodeAt():max;
        const y = String(b.c_name)?String(b.c_name).toLowerCase().charCodeAt():max;
        if (x < y) return -1
        if (x > y) return 1
      });
    }
  },
  sortReverse(state) {
    state.companies.reverse();
  },
  sortDates(state, type) {
    state.companies.sort((el1, el2) => {
      const a = moment(el1[type], 'DD.MM.YYYY HH:mm');
      const b = moment(el2[type], 'DD.MM.YYYY HH:mm');
      if (moment.max(a, b) === a) return 1;
      return -1;
    });
  },
  sortStatus() {
    state.companies.sort((a, b) => {
      return a.status - b.status;
    });
  },
  setPhoneValue(state, [val, idx]) {
    state.company.phones[idx].phone = val;
  },
  setEmailValue(state, [val, idx]) {
    state.company.emails[idx].email = val;
  },
  closePhoneDrop(state, payload) {
    const idx = state.company.phones.findIndex(({ id }) => payload === id);
    state.company.phones[idx].drop = false;
  },
};

const getters = {
  getCompanyById: (state) => (id) => {
    return state.companies.find(item => item.id === id);
  },
  getCompany: (state) => {
    if (state.showId) {
      return state.companies.find(item => item.id === state.showId);
    }
    return '';
  },
  getCompanyUrl: (state) => (id) => {
    const company = state.companies.find(item => item.id === id);
    return company.pageUrl;
  },
  companies: (state) => {
    if (state.search) {
      return state.companies.filter((item) => {
        if (item.c_name) {
          return String(item.c_name).toLowerCase().indexOf(String(state.search).toLowerCase()) !== -1;
        }
        return state.search === item.c_name;
      });
    }
    return state.companies;
  },
  getLanguages: (state) => {
    if (state.company.languages.length) {
      return state.company.languages.filter(e => e.status);
    }
    return [];
  },
  isStatus: state => state.company.status === 1,
  getUrl: (state) => {
    let url = '';
    if (state.company.settings && state.company.settings[0]) {
      url = state.company.settings[0].url;
    } else if (state.showId && state.companies.length) {
      const company = state.companies.find(item => item.id === state.showId);
      if (company && company.settings && company.settings[0]) url = company.settings[0].url;
    }
    return url;
  },
};

const actions = {
  sortCreated({ commit }) {
    commit('sortDates', 'created_at');
  },
  sortActivity({ commit }) {
    commit('sortDates', 'updated_at');
  },
  getCompanies({ commit }) {
    commit('gears/showLoading', null, {root: true});
    http.get(api.companies).then((response) => {
      commit('gears/hideLoading', null, {root: true});
      if (response.status === 200) {
        const newData = response.data.data.map((item) => {
          item.checked = false;
          if (!item.user) {
            item.user = { role: 'user' }
          }
          const zone = moment.tz.guess(true);
          item.created_at = moment(item.created_at).isValid() ? moment.tz(item.created_at, 'YYYY-MM-DD HH:mm:ss', 'GMT').tz(zone).format('DD.MM.YYYY HH:mm') : '';
          item.updated_at = moment(item.updated_at).isValid() ? moment.tz(item.updated_at, 'YYYY-MM-DD HH:mm:ss', 'GMT').tz(zone).format('DD.MM.YYYY HH:mm') : '';
          return item;
        })
        const result = newData.filter((item) => {
          return item.user && typeof item.user === 'object' ? item.user.role !== 'admin' : true
        });
        commit('updateCompanies', result);
      }
    }).catch((error) => {
      commit('gears/hideLoading', null, {root: true});
    });
  },
  getCompanyById({ commit }, id) {
    commit('gears/showLoading', null, {root: true});
    commit('setId', id);
    return new Promise((resolve, reject) => {
      http.get(api.companyById(id)).then((response) => {
        commit('gears/hideLoading', null, {root: true});
        if (response.status === 200) {
          if (!response.data.data.user) {
            response.data.data.user = state.personal;
          }
          commit('updatePersonalData', response.data.data.user);
          commit('updateCompany', response.data.data);
          resolve(response.data.data.url);
        } else {
          reject(response.data.message);
        }
      }).catch(({ response }) => {
        commit('gears/hideLoading', null, {root: true});
        reject(response);
      });
    })
  },
  getCompany({ state, commit }) {
    commit('gears/showLoading', null, {root: true});
    commit('setId', state.company.id);
    return new Promise((resolve, reject) => {
      http.get(api.companyById(state.company.id)).then((response) => {
        commit('gears/hideLoading', null, {root: true});
        if (response.status === 200) {
          if (!response.data.data.user) {
            response.data.data.user = state.personal;
          }
          commit('updatePersonalData', response.data.data.user);
          commit('updateCompany', response.data.data);
          resolve();
        }
      }).catch((error) => {
        commit('gears/showLoading', null, {root: true});
        reject(error);
      });
    })
  },
  getDataPure({ state, commit }) {
    commit('gears/showLoading', null, {root: true});
    commit('setId', state.company.id);
    return new Promise((resolve, reject) => {
      http.get(api.companyById(state.company.id)).then((response) => {
        commit('gears/hideLoading', null, {root: true});
        if (response.status === 200) {
          resolve(response.data);
        }
      }).catch((error) => {
        commit('gears/hideLoading', null, {root: true});
        reject(error);
      });
    });
  },
  putData({ state }, data) {
    return new Promise((resolve, reject) => {
      http.put(api.updateData(state.company.id), data)
        .then((resp) => {
          if (resp.status === 200) {
            resolve(resp);
          } else {
            reject(resp);
          }
        })
        .catch((error) => reject(error));
    })
  },
  updateData({ state, dispatch }) {
    // const url = await dispatch('gears/formatUrl', state.company.url, { root: true });
    return new Promise((resolve, reject) => {
      const data = {
          c_name: state.company.c_name,
          official_c_name: state.company.official_c_name,
          VAT: state.company.VAT,
          address: state.company.address,
          website: state.company.website,
          сhamber_of_commerce: state.company.сhamber_of_commerce,
          latitude: state.company.latitude,
          longitude: state.company.longitude,
      };
      http.put(api.companyById(state.company.id), data)
        .then((resp) => {
          if (resp.status === 200) {
            dispatch('updateArrays')
              .then(() => resolve(resp))
              .catch(() => reject(resp));
          } else {
            reject(resp);
          }
        })
        .catch((error) => reject(error));
    })
  },
  updateArrays({ dispatch, commit }) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      commit('gears/hideLoading', null, {root: true});
      dispatch('updatePhones')
      .then(() => {
        dispatch('updateSocials')
          .then(() => {
            dispatch('updateLanguages')
              .then(() => {
                dispatch('updateEmails')
                  .then(() => resolve())
                  .catch(() => reject())
              })
              .catch(() => reject())
          })
          .catch(() => reject())
      })
      .catch(() => {
        commit('gears/hideLoading', null, {root: true});
        eject()
      })
    });
  },
  updateProfile({ state, commit }, data) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.put(api.profile(state.personal.id), data)
        .then((response) => {
          commit('gears/hideLoading', null, {root: true});
          if (response.status === 200) {
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          commit('gears/hideLoading', null, {root: true});
          reject(error);
        })
    });
  },
  updateSocials({ state }) {
    const socials = [...state.company.socials]
    const formData = Object.assign({
      socials: JSON.stringify(socials),
      company_id: state.company.id,
    });
    return new Promise((resolve, reject) => {
      http.post(api.updateSocials, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  updatePhones({ state }) {
    const phones = [...state.company.phones];
    const formData = Object.assign({phones: JSON.stringify(phones)}, {company_id: state.company.id});
    return new Promise((resolve, reject) => {
      http.post(api.updatePhones, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  getDocsAsync({ commit }) {
    commit('gears/showLoading', null, {root: true});
    return new Promise((resolve, reject) => {
      http.get(api.companyById(state.company.id)).then((response) => {
        commit('gears/hideLoading', null, {root: true});
        if (response.status === 200) {
          const { files } = response.data.data;
          commit('updateDocs', files);
          resolve();
        }
      }).catch((error) => {
        commit('gears/hideLoading', null, {root: true});
        reject();
      });
    })
  },
  updateEmails({ state }) {
    const emails = [...state.company.emails];
    const formData = Object.assign({}, {
      emails: JSON.stringify(emails),
      company_id: state.company.id,
    });
    return new Promise((resolve, reject) => {
      http.post(api.updateEmails, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  updateLanguages({ state }) {
    const languages = [...state.company.languages];
    const formData = Object.assign({}, {
      languages: JSON.stringify(languages),
      company_id: state.company.id,
    });
    return new Promise((resolve, reject) => {
      http.post(api.updateLanguages, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
  deleteCompanies({ state }) {
    const ids = [];
    state.companies.forEach((item) => {
      if (item.checked) ids.push(item.id);
    });
    const formData = {
      ids: JSON.stringify(ids),
    }
    return new Promise((resolve, reject) => {
      http.post(api.deleteCompanies, formData).then((response) => {
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      }).catch(error => reject(error))
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
