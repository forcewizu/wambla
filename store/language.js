/* eslint-disable */

import http from 'axios';
import Vue from 'vue';
import { api } from '@/api/admin';

const state = () => ({
  lang: 'en',
  id: null,
});

const mutations = {
  updateLang(state, { lang, id = null}) {
    state.lang = lang;
    state.id = id;
  },
  setDefaultLang(state, payload = 'en') {
    state.lang = payload;
  },
};

const getters = {
  getLangValue: state => state.lang === 'en'?1:2,
};

const actions = {
  updateLang({ commit }, { lang, id, vm }) {
    vm.$cookies.set('wambla-lang', lang);
    Vue.i18n.set(lang);
    commit('updateLang', { lang, id });
  },
  getLang({ dispatch }, { vm }) {
    const isAuth = vm.$cookies.get('wambla-user-token') || vm.$cookies.get('wambla-admin-token') || '';
    let lang = vm.$cookies.get('wambla-lang');
    if (isAuth) {
      http.post(api.me).then((response) => {
        if (response.status === 200) {
          const { current_lang } = response.data.user;
          lang = current_lang ? current_lang : 'en';
          const id = response.data.user.id;
          dispatch('updateLang', { lang, id, vm });
        } else {
          dispatch('updateLang', {lang, vm});
        }
      }).catch((error) => {
        dispatch('updateLang', {lang, vm});
      })
    } else {
      dispatch('updateLang', {lang, vm});
    }
  },
  setLang({ state, dispatch }, { lang, vm }) {
    const isAuth = vm.$cookies.get('wambla-user-token') || vm.$cookies.get('wambla-admin-token') || '';
    if (isAuth) {
      const data = Object.assign({}, { current_lang: lang });
      http.put(api.profile(state.id), data)
        .then((response) => {
          if (response.status === 200) {
            dispatch('updateLang', {lang, id: state.id, vm});
          } else {
            dispatch('getLang', { vm });
          }
        })
        .catch((error) => {
          dispatch('getLang', { vm });
        })
      } else {
        dispatch('updateLang', {lang, vm});
      }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
