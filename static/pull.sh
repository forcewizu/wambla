#!/usr/bin/env sh

set -e

git fetch --all

git reset --hard origin/prod