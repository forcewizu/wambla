const envTypeDev = process.env.NODE_ENV === 'development';

const fetchObject = (store, redirect) => async (objectId) => {
  try {
    const companyId = await store.dispatch('dataAgency/getObject', objectId);
    const url = await store.dispatch('dataAgency/getCompanyById', companyId);
    return redirect(`/${url}/${companyId}`);
  } catch (err) {
    throw err;
  }
}

const alertBlock = (auth, route) => {
  if (!auth && envTypeDev) {
    console.log(
      `Route "${route}" is blocked (You can see this page becouse it's development mode)`
    );
  }
};

const ifNotAuthenticated = ({ app, redirect }) => {
  const user = app.$cookies.get('wambla-user-token');
  const admin = app.$cookies.get('wambla-admin-token');
  if (!user && !admin) {
    return;
  } else if (user) {
    return redirect('/agency');
  } else if (admin) {
    return redirect('/admin');
  } else {
    return redirect('/');
  }
};

const indexRedirect = ({ app, redirect, query }) => {

  const user = app.$cookies.get('wambla-user-token');
  const admin = app.$cookies.get('wambla-admin-token');

  switch (true) {
    case object:
      return redirect(`?object=${object}&j=12`)
    case user:
      return redirect('/agency');
    case admin:
      return redirect('/admin');
    default:
      return redirect('/auth');
  }
};

const ifAdminAuthenticated = ({ app, redirect, route }) => {
  const admin = app.$cookies.get('wambla-admin-token');
  if (admin || envTypeDev) {
    alertBlock(admin, route.name);
    return;
  } else {
    return redirect('/auth');
  }
};

const ifAuthenticated = ({ app, redirect, route }) => {
  const user = app.$cookies.get('wambla-user-token');
  if (user || envTypeDev) {
    alertBlock(user, route.name);
    return;
  } else {
    return redirect('/auth');
  }
};

export default function(context) {
  const { route: { name } } = context;
  const checkRoute = i => i === name;

  const authRoutes = ['auth', 'auth-login'];
  const adminRoutes = [
    'admin',
    'admin-companies',
    'admin-companies-id',
    'admin-agency-id',
    'admin-profile',
    'admin-clients',
    'admin-users',
    'admin-translations',
    'admin-settings',
  ];
  const userRoutes = ['agency'];

  switch (true) {
    case name === 'index':
      return;
    case authRoutes.some(checkRoute):
      return ifNotAuthenticated(context);
    case adminRoutes.some(checkRoute):
      return ifAdminAuthenticated(context);
    case userRoutes.some(checkRoute):
      return ifAuthenticated(context);
    default:
      return;
  }
}
