import api from './api/public';
import axios from 'axios';

module.exports = async () => {
  /*
   ** Headers of the page
   */
  let routes = [];
  try {
    const { data } = await axios.get(api.sitemap);
    routes = data.data.map(route => `/${route}`);
  } catch (err) {
    console.log(`${api.sitemap} api not working`)
  }

  return {
    router: {
      middleware: ['auth'],
    },
    head: {
      title: 'Wambla',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Wambla' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        {
          rel: 'stylesheet',
          href: 'https://use.fontawesome.com/releases/v5.8.2/css/all.css',
          integrity:
            'sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay',
          crossorigin: 'anonymous'
        }
      ]
    },
    plugins: [
      { src: '~/plugins/axios.js' },
      { mode: 'client', src: '~/plugins/ant-design-vue.js' },
      { mode: 'client', src: '~/plugins/clientUtils.js' },
      { mode: 'client', src: '~/plugins/directives.js' },
      { src: '~/plugins/utils.js' },
      { src: '~/plugins/components.js' },
      { src: '~/plugins/i18n/vuex-i18n.js' },
      { mode: 'client', src: '~/plugins/vue-link.js' },
    ],
    /*
     ** Customize the progress bar color
     */
    loading: { color: '#3B8070' },
    /*
     ** Build configuration
     */
    modules: [
      'vue-scrollto/nuxt',
      '@nuxtjs/style-resources',
      '@nuxtjs/sitemap',
      '@nuxtjs/robots',
      ['cookie-universal-nuxt', { parseJSON: false }]
    ],
    robots: {
      UserAgent: '*',
      Disallow: '/'
    },
    sitemap: {
      hostname: 'https://wambla.com',
      exclude: [
        '/secret',
        '/admin',
        '/admin/**'
      ],
      routes,
    },
    styleResources: {
      scss: ['~/assets/scss/style.scss']
    },
    css: [{ src: '~assets/scss/main.scss', lang: 'scss' }],
    build: {
      /*
       ** Run ESLint on save
       */
      transpile: ['vue-link', 'ant-design-vue'],
      extend(config, { isDev, isClient }) {
        if (isDev && isClient) {
          config.module.rules.push({
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/
          });
        }
      }
    }
  }
};
