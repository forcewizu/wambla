const https = 'https://wambla-api.com/api';
const http = 'http://wambla-api.com/api';

const url = https;

const api = {
  companyByUrl: `${url}/companies/byurl`,
  companyById: (id) => `${url}/companies/${id}`,
  objects: `${url}/pobjects`,
  avatar: `${url}/avatar`,
  logo: `${url}/logo`,
  logo_big: `${url}/big-logo`,
  team_avatar: `${url}/landing/team-avatar`,
  getFullObject: `${url}/one-object`,
  getApiFullObject: (company_id, object_id) => {
    return `${url}/one-object?company_id=${company_id}&object_id=${object_id}`;
  },
  getObject: (id) => `${url}/object/${id}`,
  sitemap: `${url}/sitemap`,
}

export { api };
export default api;