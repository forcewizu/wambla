const https = 'https://wambla-api.com/api';
const http = 'http://wambla-api.com/api';

const url = https;

const api = {
  login: `${url}/login`,
  signup: `${url}/register`,
  me: `${url}/me`,
  updateData(id) {
    return `${url}/companies/${id}`;
  },
  uploadAvatar: `${url}/avatar`,
  uploadLogo: `${url}/logo`,
  uploadDocs: `${url}/file`,
  updatePhones: `${url}/phones`,
  updateEmails: `${url}/emails`,
  updateLanguages: `${url}/languages`,
  updateSocials: `${url}/socials`,
  profile(id) {
    return `${url}/users/${id}`;
  },
  deleteFile(id) {
    return `${url}/images/${id}`;
  },
  recovery: `${url}/forgot`,
  landing: `${url}/landing`,
  team: `${url}/companies/team`,
  schedule: `${url}/companies/schedule`,
  photo: `${url}/logo-big `,
  memberDelete: `${url}/companies/team-delete`,
  posting: 'https://social.wambla.com/auth/logout',
  feedback: `${url}/feedback`,
  logo: `${url}/logo`,
  logo_big: `${url}/big-logo`,
  avatar: `${url}/avatar`,
  team_avatar: `${url}/landing/team-avatar`,
  changeUrl: `${url}/admin-change-url`,
};

export { api };