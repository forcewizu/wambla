const https = 'https://wambla-api.com/api';
const http = 'http://wambla-api.com/api';

const url = https;

const api = {
  companies: `${url}/companies`,
  companyById(id) {
    return `${url}/companies/${id}`;
  },
  updateData(id) {
    return `${url}/companies/${id}`;
  },
  me: `${url}/me`,
  ban(id) {
    return `${url}/companies/${id}/ban`;
  },
  unban(id) {
    return `${url}/companies/${id}/unban`;
  },
  profile(id) {
    return `${url}/users/${id}`;
  },
  uploadAvatar: `${url}/avatar`,
  uploadAvatarAgency: `${url}/admin/avatar`,
  uploadLogo: `${url}/logo`,
  uploadDocs: `${url}/file`,
  updatePhones: `${url}/phones`,
  updateEmails: `${url}/emails`,
  updateLanguages: `${url}/languages`,
  updateSocials: `${url}/socials`,
  deleteFile(id) {
    return `${url}/images/${id}`;
  },
  addCompany: `${url}/companies/admin`,
  deleteCompanies: `${url}/companies/delete `,
  parsing: `${url}/parsing`,
  objects: `${url}/pobjects`,
};

export { api };