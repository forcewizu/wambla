import { api } from '../api/public';
const { team_avatar, avatar } = api;
import http from 'axios';

const crop = {
  data() {
    return {
      previewImage: '',
      newFile: '',
      loading: {
        crop: false,
      },
      isError: false,
      errorText: '',
      cropModal: false,
      backup: {
        avatar: '',
        logo: '',
      },
    };
  },
  methods: {
    showError() {
      if (this.isError) {
        this.isError = false;
        setTimeout(() => {
          this.isError = true;
          setTimeout(() => {
            this.isError = false;
          }, 3000);
        }, 20);
      } else {
        this.isError = true;
        setTimeout(() => {
          this.isError = false;
        }, 3000);
      }
    },
    zoomIn() {
      this.$refs.cropper.relativeZoom(0.1);
    },
    zoomOut() {
      this.$refs.cropper.relativeZoom(-0.1);
    },
    preCropImage() {
      this.loading.crop = true;
      setTimeout(() => {this.cropImage()}, 20);
    },
    async setImage(e) {
      const file = this.newFile = e.target.files[0];
      if (!file.type.includes('image/')) {
        this.errorText = 'Please select an image file';
        this.showError();
        return;
      }
      // this.resetForm();
      await this.$nextTick();
      if (typeof FileReader === 'function') {
        const reader = new FileReader();
        reader.onload = (event) => {
          this.previewImage = event.target.result;
          this.backup.avatar = event.target.result;
          this.backup.logo = event.target.result;
          this.cropModal = true;
        };
        reader.readAsDataURL(file);
      } else {
        this.errorText = 'Sorry, FileReader API not supported';
        this.showError();
      }
    },
    resetForm() {
      document.getElementById('form-image').reset();
    },
    editAvatar(id) {
      const api = avatar;
      const data = {
        params: {
          user_id: id,
        },
      };
      http.get(api, data)
        .then(resp => {
          if (this.backup.avatar) {
            this.previewImage = this.backup.avatar;
            this.cropModal = true;
          } else {
            this.editCurrentImage(resp.data.logo);
          }
        })
    },
    editTeamAvatar(id, order) {
      this.$store.commit('gears/showLoading');
      const api = team_avatar;
      const data = {
        params: {
          company_id: id,
          order_id: order,
        },
      };
      http.get(api, data)
        .then(resp => {
          this.$store.commit('gears/hideLoading');
          if (this.backup.avatar) {
            this.previewImage = this.backup.avatar;
            this.cropModal = true;
          } else {
            this.editCurrentImage(resp.data.logo);
          }
        })
        .catch((error) => {
          this.$store.commit('gears/hideLoading');
        })
    },
    editCurrentImage(image) {
      const file = `data:image/png;base64,${image}`;
      this.previewImage = file;
      this.cropModal = true;
    },
    rotate() {
      this.$refs.cropper.rotate(90);
    },
    dataURItoBlob(dataURI) {
      const binary = atob(dataURI.split(',')[1]);
      const array = [];
      for(let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
    },
  },
}

export default crop;