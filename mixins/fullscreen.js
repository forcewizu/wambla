const fullscreenMixin = {
  data() {
    return {
      fullscreen: false,
    };
  },
  methods: {
    openFullscreen() {
      this.fullscreen = true;
      if (this.$el.requestFullscreen) {
        this.$el.requestFullscreen();
      } else if (this.$el.mozRequestFullScreen) { /* Firefox */
        this.$el.mozRequestFullScreen();
      } else if (this.$el.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        this.$el.webkitRequestFullscreen();
      } else if (this.$el.msRequestFullscreen) { /* IE/Edge */
        this.$el.msRequestFullscreen();
      }
    },
    closeFullscreen() {
      this.fullscreen = false;
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
      }
    }
  }
}

export default fullscreenMixin;