import { api } from '../api/public';
const { avatar, logo, logo_big, team_avatar} = api;
import http from 'axios';

const crop = {
  data() {
    return {
      loading: {
        crop: false,
        save: false,
        logo: false,
      },
      previewImage: '',
      imageType: '',
      cropModal: false,
      backup: {
        avatar: '',
        logo: '',
      },
      fileName: 'image.png',
      fileType: 'image/png',
    };
  },
  methods: {
    editAvatar(id) {
      this.$store.commit('gears/showLoading');
      const api = avatar;
      const data = {
        params: {
          user_id: id,
        },
      };
      http.get(api, data)
        .then(resp => {
          this.$store.commit('gears/hideLoading');
          this.editCurrentImage(resp.data.logo, 'avatar');
        })
        .catch((error) => {
          this.$store.commit('gears/hideLoading');
        })
    },
    editLogo(id) {
      const api = logo;
      const data = {
        params: {
          company_id: id,
        },
      };
      http.get(api, data)
        .then(resp => {
          this.editCurrentImage(resp.data.logo, 'logo');
        })
    },
    editLogoBig(id) {
      const api = logo_big;
      const data = {
        params: {
          company_id: id,
        },
      };
      http.get(api, data)
        .then(resp => {
          this.editCurrentImage(resp.data.logo, 'logo_big');
        })
    },
    setImage(e, type) {
      if (type === 'logo') this.loading.logo = true;
      this.imageType = type;
      const file = e.target.files[0];
      this.fileName = file.name;
      this.fileType = file.type;
      if (!file.type.includes('image/')) {
        this.showError();
        this.loading.logo = false;
        return;
      }
      if (typeof FileReader === 'function') {
        const reader = new FileReader();
        reader.onload = (event) => {
          this.previewImage = event.target.result;
          this.backup[type] = event.target.result;
          this.loading.logo = false;
          this.cropModal = true;
        };
        reader.readAsDataURL(file);
        this.resetForm();
      } else {
        this.showError();
        this.loading.logo = false;
      }
    },
    resetForm() {
      
    },
    editCurrentImage(image, type) {
      this.imageType = type;
      if (this.backup[type]) {
        this.previewImage = this.backup[type];
        this.cropModal = true;
      } else {
        const file = `data:image/png;base64,${image}`;
        this.previewImage = file;
        this.cropModal = true;
      }
    },
    arrayBufferToBase64(buffer) {
      var binary = '';
      var bytes = [].slice.call(new Uint8Array(buffer));
      bytes.forEach((b) => binary += String.fromCharCode(b));
      return window.btoa(binary);
    },
    editCurrentImageBlob(image, type) {
      this.imageType = type;
      this.previewImage = image;
      this.cropModal = true;
    },
    rotate() {
      this.$refs.cropper.rotate(90);
    },
    preCropImage() {
      this.loading.crop = true;
      setTimeout(() => {this.cropImage()}, 20);
    },
    zoomIn() {
      this.$refs.cropper.relativeZoom(0.1);
    },
    zoomOut() {
      this.$refs.cropper.relativeZoom(-0.1);
    },
    cropImage: async function() {
      this.loading.crop = true;
      const URL = this.$refs.cropper.getCroppedCanvas().toDataURL();
      const file = await this.dataURItoBlobAsync(URL);
      if (this.imageType === 'logo') {
        this.loadLogo(file);
      } else {
        this.loadAvatar(file);
      }
    },
    toFile(theBlob) {
      theBlob.lastModifiedDate = new Date();
      theBlob.name = this.fileName;
      return theBlob;
    },
    dataURItoBlob(dataURI) {
      const binary = atob(dataURI.split(',')[1]);
      const array = [];
      for(let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], {type: this.fileType});
    },
    dataURItoBlobAsync(dataURI) {
      return new Promise((resolve, reject) => {
        const binary = atob(dataURI.split(',')[1]);
        const array = [];
        for(let i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        const blob = new Blob([new Uint8Array(array)], {type: this.fileType});
        const image = new File([blob], this.fileName, { type: this.fileType, lastModified: Date.now() });
        resolve(image);
      })
    },
  }
}

export default crop;