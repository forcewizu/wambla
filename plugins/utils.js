import Vue from 'vue';
import validation from '../assets/js/validation';

Vue.prototype.$validation = validation;
