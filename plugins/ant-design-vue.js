import Vue from 'vue';
import {
  Tooltip,
  Button,
  message,
  notification,
  Dropdown,
  Menu,
  Icon,
  Spin
} from 'ant-design-vue';
import '../assets/css/ant.css';

Vue.use(Tooltip);
Vue.use(Dropdown);
Vue.use(Menu);
Vue.use(Spin);
Vue.use(Icon);
Vue.use(Button);

notification.config({
  duration: 0,
});

Vue.prototype.$message = message;
Vue.prototype.$notification = notification;

