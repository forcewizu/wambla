import Vue from 'vue';
import VueLazyload from 'vue-lazyload';
import EditButton from '../components/common/buttons/EditButton.vue';
import VueFoldable from 'vue-foldable';
import VueScrollTo from 'vue-scrollto';
import 'vue-glide-js/dist/vue-glide.css';
import '../assets/scss/main.scss';

Vue.component('foldable', VueFoldable);
Vue.component('EditButton', EditButton);

Vue.use(VueScrollTo)
Vue.use(VueLazyload, {
  error: require('~/assets/images/error.jpg'),
})
