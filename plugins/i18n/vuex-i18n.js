import Vue from 'vue';
import Vuex from 'vuex';
import VuexI18n from 'vuex-i18n/dist/vuex-i18n.umd.js';

export default ({ app }) => {
  const store = new Vuex.Store();

  Vue.use(VuexI18n.plugin, store);

  Vue.i18n.add('en', require('./en.json'));
  Vue.i18n.add('nl', require('./nl.json'));

  let userLang = 'en';

  userLang =
    app.$cookies.get('wambla-lang');
  if (process.client) {
    userLang =
      app.$cookies.get('wambla-lang') || navigator.language.slice(0, 2);
  }
  const lang = Vue.i18n.locales().find(item => {
    return item === userLang;
  });

  lang ? Vue.i18n.set(lang) : Vue.i18n.set('en');

  Vue.i18n.fallback('en');
};
