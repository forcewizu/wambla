import http from 'axios';

export default ({ app }) => {
  const token =
    app.$cookies.get('wambla-user-token') ||
    app.$cookies.get('wambla-admin-token');
  if (token) {
    http.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  }
}
