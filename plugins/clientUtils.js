import Vue from 'vue';
import VueNumerals from 'vue-numerals';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueCropper from 'vue-cropperjs';

import VueTimepicker from 'vue2-timepicker';
import SocialSharing from 'vue-social-sharing';
import VueGlide from 'vue-glide-js';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAq53Il3wxDCSKNGPFoYwKUK5YLndqcDtU',
    libraries: 'places',
  },
});
Vue.use(SocialSharing);
Vue.use(VueNumerals);
Vue.use(VueGlide);

Vue.component('vue-cropper', VueCropper);

Vue.component('VueTimepicker', VueTimepicker);